# aiMotive tesztfeladat

## Projekt futtatása
### Függőségek telepítése
```
npm install
```

### Futtatás fejlesztéshez 
```
npm run serve
```

### Éles telepítéshez fordítás
```
npm run build
```

## Leírás
A webalkalmazás egy pontfelhőt jelenít meg, illetve az a pontfelhőben található járművek határoló 3D dobozait.

A felületen lehetséges a megjelenített adatokat forgatni, nagyítani, mozgatni.  Vezérlés: nagyítás: görgetés, forgatás: bal klikk + kurzor, mozgatás: jobb klikk + kurzor.

A felületen található "Megjelenítendő intervallum" csúszkával lehetséges a megjelenített pontokat szűkíteni, a felsorolás elejéről és/vagy végéről pontokat elhagyni.

A weboldal reszponzív, de alapvetően 1080P felbontásra van optimalizálva.

## Feltételezések
- A webszolgáltatás a pontfelhőt JSON formátumban adja vissza.
- A 3D dobozok JSON formátumban vannak, amely tartalmazza a szélességet, magasságot és hosszúságot, illetve a középpont koordinátáit.
